//
//  PeliculasGuardadasViewController.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class PeliculasGuardadasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var arrayMovieSaved:[Movie] = []
    
    
    @IBOutlet weak var moviesSavedTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let bm = BackendManager()
        bm.getAllMoviesSaved()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("getMoviesSaved"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let bm = BackendManager()
        bm.getAllMoviesSaved()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("getMoviesSaved"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData(_ notification:Notification) {
        guard let arrayMovie = notification.userInfo?["movieArray"] as? [Movie] else {
            return
        }
        self.arrayMovieSaved = arrayMovie
        moviesSavedTableView.reloadData()
    }

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMovieSaved.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.moviesSavedTableView.dequeueReusableCell(withIdentifier: "movieCellSaved") as? PeliculaGuardadaCeldaTableViewCell
        
        cell?.movieObj = self.arrayMovieSaved[indexPath.row]
        cell?.fillData()
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mostrarDetalles2"{
            let destinationController = segue.destination as! DetallepeliculaGuardadaViewController
            
            
            if let indexPath = self.moviesSavedTableView.indexPathForSelectedRow {
                let selectedMovie = arrayMovieSaved[indexPath.row]
                destinationController.currentMovieSaved = selectedMovie
                
            }
        }
    }
    
    
    

    }
