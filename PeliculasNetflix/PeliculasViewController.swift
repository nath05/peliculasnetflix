//
//  PeliculasViewController.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class PeliculasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating {
    
    
    var arrayMovie:[Movie] = []
    @IBOutlet weak var movieTableView: UITableView!
    
    var resultSearchController : UISearchController!
    var peliculaFiltrada: [Movie] = []
    
    var isSearchactive = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let bm = BackendManager()
        bm.getAllMovies()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("getMovies"), object: nil)
        // Do any additional setup after loading the view.
        confSearchBar()
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func reloadData(_ notification:Notification) {
        guard let arrayMovie = notification.userInfo?["movieArray"] as? [Movie] else {
            return
        }
        self.arrayMovie = arrayMovie
        movieTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchactive{
            return peliculaFiltrada.count
        } else {
            return arrayMovie.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.movieTableView.dequeueReusableCell(withIdentifier: "movieCell") as? PeliculaCeldaTableViewCell
        
        if isSearchactive{
            cell?.movieObj = peliculaFiltrada[indexPath.row]
        }else{
            cell?.movieObj = self.arrayMovie[indexPath.row]
        }
        
        
        cell?.fillData()
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mostrarDetalles"{
            let destinationController = segue.destination as! DetallePeliculaViewController
            

            if let indexPath = self.movieTableView.indexPathForSelectedRow {
                let selectedMovie = arrayMovie[indexPath.row]
                destinationController.currentMovie = selectedMovie
                
            }
        }
    }
    
    func confSearchBar(){
    
        resultSearchController = UISearchController (searchResultsController: nil)
        resultSearchController.searchResultsUpdater = self
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchBar.searchBarStyle = .prominent
        resultSearchController.searchBar.sizeToFit()
        movieTableView.tableHeaderView = resultSearchController.searchBar
    
    
    }
    
    
    
    func updateSearchResults(for searchController: UISearchController) {
        var searchText = searchController.searchBar.text?.lowercased()
        
        if searchController.searchBar.text !=  ""{
            
            
            isSearchactive = true
            peliculaFiltrada = arrayMovie.filter{
                ($0.movieTitle?.lowercased().contains(searchText!))!
            }
            
        } else {
            isSearchactive = false
        }
        movieTableView.reloadData()
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchactive = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchactive = false
        movieTableView.reloadData()
    }


}
