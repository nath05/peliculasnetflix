//
//  TabBarViewController.swift
//  PeliculasNetflix
//
//  Created by Nathy Cumbicos on 6/14/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
              // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        let items = tabBar.items
        
        items?[0].title="Peliculas API"
        items?[1].title="Peliculas BackEnd"
        
        items?[0].image=#imageLiteral(resourceName: "Nube")
        items?[1].image=#imageLiteral(resourceName: "Celular")
    }

}
