//
//  BackendManager.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

//Clase que permite consumir datos de API y BackEnd

class BackendManager {

    // GET: - Peliculas de la API
    func getAllMovies () {
        
        let url = "http://netflixroulette.net/api/api.php?actor=Nicolas%20Cage"
        
        Alamofire.request(url).responseArray { (response: DataResponse<[Movie]>) in
                
                let movieResponse = response.result.value
                
                if let movieArray = movieResponse{
                    NotificationCenter.default.post(name: NSNotification.Name("getMovies"), object: nil, userInfo: [ "movieArray" : movieArray ])
                }

            }

        }
    
    // GET: - Peliculas guardadas en el BackEnd
    func getAllMoviesSaved () {
        
        let url = "https://netflixcumdav.herokuapp.com/Peliculas"
        
        Alamofire.request(url).responseArray { (response: DataResponse<[Movie]>) in
            
            let movieResponse = response.result.value
            
            if let movieArray = movieResponse{
                NotificationCenter.default.post(name: NSNotification.Name("getMoviesSaved"), object: nil, userInfo: [ "movieArray" : movieArray ])
            }
            
        }
        
    }
    
    //GET: - Imagen del objeto Json de API
    func getImage (_ url : String, completionHandler: @escaping(UIImage)->()){
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
    
    //POST: - Guardar pelicula en el BackEnd
    func insertarMovie(movie:Movie){
        
        let imagenUrl = movie.moviePoster
        
        let parametros = ["show_title":movie.movieTitle, "category":movie.movieCategory, "director":movie.movieDirector, "poster":movie.moviePoster, "show_cast":movie.movieCast, "release_year":movie.movieYear, "summary":movie.movieSummary]
        
        let urlString = "https://netflixcumdav.herokuapp.com/Peliculas"
        
        Alamofire.request(urlString, method: .post, parameters: parametros,
                          encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                            print(response)
        }
    }
}
