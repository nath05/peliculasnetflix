//
//  PeliculaCeldaTableViewCell.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class PeliculaCeldaTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var LabelTitulo: UILabel!
    
    @IBOutlet weak var LabelDirector: UILabel!
    
    @IBOutlet weak var ImagenPortada: UIImageView!
    
    var movieObj:Movie!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func fillData() {
        self.LabelTitulo.text = self.movieObj.movieTitle
        self.LabelDirector.text = self.movieObj.movieDirector
        
        if movieObj.movieImage == nil {
            
            let bm = BackendManager()
            
            bm.getImage((movieObj?.moviePoster)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.ImagenPortada.image = imageR
                }
                
            })
            
        } else {
            
            ImagenPortada.image = movieObj.movieImage
            
        }
    }
}
