//
//  DetallepeliculaGuardadaViewController.swift
//  PeliculasNetflix
//
//  Created by Nathy Cumbicos on 15/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class DetallepeliculaGuardadaViewController: UIViewController {

    var currentMovieSaved:Movie!
    
    
    @IBOutlet weak var LabelDetalleTitulo: UILabel!
    @IBOutlet weak var LabelDetalleDirector: UILabel!
    
    
    @IBOutlet weak var LabelDetalleResumen: UILabel!
    @IBOutlet weak var LabelDetalleAño: UILabel!
    
    @IBOutlet weak var LabelDetalleCategoria: UILabel!
    
    @IBOutlet weak var PortadaDetalleImagen: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LabelDetalleTitulo.text = currentMovieSaved.movieTitle!
        LabelDetalleDirector.text = currentMovieSaved.movieDirector!
        LabelDetalleResumen.text = currentMovieSaved.movieSummary
        LabelDetalleAño.text = currentMovieSaved.movieYear
        LabelDetalleCategoria.text = currentMovieSaved.movieCategory
        
        if currentMovieSaved.movieImage == nil {
            
            let bm = BackendManager()
            
            bm.getImage((currentMovieSaved?.moviePoster)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.PortadaDetalleImagen.image = imageR
                }
                
            })
            
        } else {
            
            PortadaDetalleImagen.image = currentMovieSaved.movieImage
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
