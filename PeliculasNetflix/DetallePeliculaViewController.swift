//
//  DetallePeliculaViewController.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class DetallePeliculaViewController: UIViewController {

    @IBOutlet weak var LabelDetalleTitulo: UILabel!
    @IBOutlet weak var LabelDetalleDirector: UILabel!
    @IBOutlet weak var PortadaDetalleImagen: UIImageView!
    @IBOutlet weak var LabelDetalleResumen: UILabel!
    @IBOutlet weak var LabelDetalleAño: UILabel!
    @IBOutlet weak var LabelDetalleCategoria: UILabel!
    
    var currentMovie:Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LabelDetalleTitulo.text = currentMovie.movieTitle!
        LabelDetalleDirector.text = currentMovie.movieDirector!
        LabelDetalleResumen.text = currentMovie.movieSummary
        LabelDetalleAño.text = currentMovie.movieYear
        LabelDetalleCategoria.text = currentMovie.movieCategory
        
        if currentMovie.movieImage == nil {
            
            let bm = BackendManager()
            
            bm.getImage((currentMovie?.moviePoster)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.PortadaDetalleImagen.image = imageR
                }
                
            })
            
        } else {
            
            PortadaDetalleImagen.image = currentMovie.movieImage
            
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func guardarButton(_ sender: Any) {
        let bm = BackendManager()
        bm.insertarMovie(movie: currentMovie)
    }
    
    
    


}
