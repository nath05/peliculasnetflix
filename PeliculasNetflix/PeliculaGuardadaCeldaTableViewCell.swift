//
//  PeliculaGuardadaCeldaTableViewCell.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class PeliculaGuardadaCeldaTableViewCell: UITableViewCell {

    @IBOutlet weak var ImagenPeliculaGuardada: UIImageView!
    @IBOutlet weak var LabelTitulo: UILabel!
    @IBOutlet weak var LabelCategoria: UILabel!
    
    var movieObj:Movie!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData() {
        self.LabelTitulo.text = self.movieObj.movieTitle
        self.LabelCategoria.text = self.movieObj.movieCategory
        
        if movieObj.movieImage == nil {
            
            let bm = BackendManager()
            
            bm.getImage((movieObj?.moviePoster)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.ImagenPeliculaGuardada.image = imageR
                }
                
            })
            
        } else {
            
            ImagenPeliculaGuardada.image = movieObj.movieImage
            
        }
    }

}
