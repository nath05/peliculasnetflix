//
//  Movie.swift
//  PeliculasNetflix
//
//  Created by David Davila on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import ObjectMapper

// Clase que permite mapear de un objeto JSON a un objeto Swift

class Movie : Mappable {
    
    var movieTitle: String?
    var movieCategory: String?
    var movieDirector: String?
    var moviePoster: String?
    var movieImage: UIImage?
    var movieCast: String?
    var movieSummary: String?
    var movieYear: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping (map: Map) {
        movieTitle <- map ["show_title"]
        movieCategory <- map ["category"]
        movieDirector <- map ["director"]
        moviePoster <- map ["poster"]
        movieCast <- map ["show_cast"]
        movieYear <- map ["release_year"]
        movieSummary <- map ["summary"]
    }
}
